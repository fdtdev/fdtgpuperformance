# FDT GPU Performance

Generic scroll implementation with infinite option and async loading capacity


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.gpuperformance": "https://bitbucket.org/fdtdev/fdtgpuperformance.git#2.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtgpuperformance/src/2.0.0/LICENSE.md)