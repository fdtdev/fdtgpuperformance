﻿using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

namespace com.FDT.GPUPerformance
{
	/*
	 * 		Contributors:	
	 * 		Description:	
	 */
	[CreateAssetMenu(menuName = "FDT/GPUPerformance/GPUPerformanceAsset", fileName = "GPUPerformanceAsset")]
	public class GPUPerformanceAsset : ResetScriptableObject
	{
		#region Classes
		[System.Serializable]
		public class GPUData
		{
			public string model;
			public int maxResolutionWidth;
			public int maxResolutionHeight;
		}
		#endregion

		#region GameEvents

		//[Header("GameEvents"), SerializeField] 

		#endregion

		#region Actions

		#endregion

		#region UnityEvents

		//[Header("UnityEvents"), SerializeField] 

		#endregion

		#region Fields

		public List<GPUData> list = new List<GPUData>();

		#endregion

		#region Properties

		#endregion

		#region Methods
		public GPUData GetGPUData(string model)
		{
			model = model.ToLower(CultureInfo.InvariantCulture);
			for (int i = 0; i < list.Count; i++)
			{
				string gpuModel = list[i].model.ToLower(CultureInfo.InvariantCulture);
				Debug.Log("comparing "+model+" with "+gpuModel);
				if (model.Contains(gpuModel))
				{
					Debug.Log("found gpu model "+gpuModel);
					return list[i];
				}
			}

			return null;
		}
		#endregion
	}
}