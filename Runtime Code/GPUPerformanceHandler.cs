﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.FDT.GPUPerformance
{
	/*
	 * 		Contributors:	
	 * 		Description:	
	 */
	public class GPUPerformanceHandler : MonoBehaviour
	{
		#region Classes
		public enum ResizeMethod
		{
			USE_HEIGHT=0, USE_WIDTH =1
		}
		#endregion

		#region GameEvents

		//[Header("GameEvents"), SerializeField] 

		#endregion

		#region Actions

		#endregion

		#region UnityEvents

		//[Header("UnityEvents"), SerializeField] 

		#endregion

		#region Fields

		[Header("Fields"), SerializeField] protected GPUPerformanceAsset _gpuPerformanceAsset;

		[SerializeField] protected ResizeMethod resizeMethod;

		#endregion

		#region Properties

		#endregion

		#region Methods
		void Awake ()
		{
			string gpuDeviceName = SystemInfo.graphicsDeviceName;
			Debug.Log("looking for "+gpuDeviceName+" in devices");
			var gpudata = _gpuPerformanceAsset.GetGPUData(gpuDeviceName);
			if (gpudata != null)
			{
				Debug.Log("using gpu data to set resolution");
				SetResolution(gpudata);
			}
		}

		private void SetResolution(GPUPerformanceAsset.GPUData gpudata)
		{
			if (resizeMethod == ResizeMethod.USE_WIDTH)
			{
				SetResolutionWidth(gpudata);
			}
			else
			{
				SetResolutionHeight(gpudata);
			}
		
		}

		private void SetResolutionWidth(GPUPerformanceAsset.GPUData gpudata)
		{
			int width = Screen.width;
			int height = Screen.height;

			if (width > gpudata.maxResolutionWidth)
			{
				height = (int)(height * ((float)gpudata.maxResolutionWidth/(float)width));
				width = gpudata.maxResolutionWidth;
			
			}
			Debug.Log("original resolution was "+Screen.width+"-"+Screen.height+", using "+width+"-"+height);
			Screen.SetResolution(width, height, true);
		}

		private void SetResolutionHeight(GPUPerformanceAsset.GPUData gpudata)
		{
			int width = Screen.width;
			int height = Screen.height;

			if (height > gpudata.maxResolutionHeight)
			{
				width = (int)(width * ((float)gpudata.maxResolutionHeight/(float)height));
				height = gpudata.maxResolutionHeight;

			}
			Debug.Log("original resolution was "+Screen.width+"-"+Screen.height+", using "+width+"-"+height);
			Screen.SetResolution(width, height, true);
		}
		#endregion
	}
}